const parseToken = ({ key, balanceUSD, context }) => {
  const { symbol } = context;
  return { key, symbol, balanceUSD };
};

const parseTokensFromWallet = (wallet) => {
  let tokens = [];
  let network = "ethereum";
  Object.keys(wallet).forEach((value) => {
    network = wallet[value].network;
    tokens.push(parseToken(wallet[value]));
  });
  return { network, tokens };
};

const parseUniV3Position = ({ displayProps, balanceUSD, breakdown }) => {
  const { label } = displayProps;
  const supplied = breakdown.filter(({ metaType }) => metaType === "supplied");
  let tokens = [];
  supplied.forEach(({ balanceUSD, context }) => {
    const { symbol } = context;
    tokens.push({
      symbol,
      balanceUSD,
    });
  });
  return {
    label,
    balanceUSD,
    tokens,
  };
};

const parseUniV3Positions = (positions) => {
  let liquidityPositions = [];
  positions.forEach((rawPosition) => {
    liquidityPositions.push(parseUniV3Position(rawPosition));
  });
  return liquidityPositions;
};

export const generateUrl = (addresses) => {
  let url = `https://api.zapper.fi/v2/balances?`;
  addresses.forEach((address, _index) => {
    url += `addresses[]=${address}${
      _index === addresses.length - 1 ? "" : "&"
    }`;
  });
  return encodeURI(url);
};

export const generateEventSourceDict = (apiKey) => {
  return {
    withCredentials: true,
    headers: {
      "Content-Type": "text/event-stream",
      "User-Agent": "Mozilla/5.0",
      Authorization: `Basic ${Buffer.from(`${apiKey}:`).toString("base64")}`,
    },
  };
};

export const parse = (type, object) => {
  switch (type) {
    case "tokens":
      return parseTokensFromWallet(object);
    case "uniswap-v3":
      return parseUniV3Positions(object);
    default:
      return;
  }
};
