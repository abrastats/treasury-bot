export const getWalletValue = (tokensBalances) =>
  tokensBalances.reduce(
    (acc, { tokens }) =>
      acc + tokens.reduce((balance, { balanceUSD }) => balance + balanceUSD, 0),
    0
  );

export const getPositionsValue = (protocolsBalances) =>
  protocolsBalances.reduce((acc, { balanceUSD }) => acc + balanceUSD, 0);
