import { Client, Intents } from "discord.js";
import EventSource from "eventsource";
import { config } from "./config.js";
import { getPositionsValue, getWalletValue } from "./utils/helpers.js";
import { generateEventSourceDict, generateUrl, parse } from "./utils/zapper.js";

const client = new Client({ intents: [Intents.FLAGS.GUILDS] });

let protocolsBalances = [];
let tokensBalances = [];

const handleTokenBalance = (tokenBalance) => {
  if (
    tokensBalances.map((item) => item.network).includes(tokenBalance.network)
  ) {
    const index = tokensBalances.findIndex(
      (item) => item.network === tokenBalance.network
    );
    if (tokensBalances[index].balanceUSD !== tokenBalance.balanceUSD) {
      tokensBalances[index] = tokenBalance;
    }
  } else {
    tokensBalances.push(tokenBalance);
  }
};

const handleProtocolPosition = (protocolObject) => {
  if (protocolsBalances.map((item) => item.key).includes(protocolObject.key)) {
    const index = protocolsBalances.findIndex(
      (item) => item.key === protocolObject.key
    );
    if (protocolsBalances[index].balanceUSD !== protocolObject.balanceUSD) {
      protocolsBalances[index] = protocolObject;
    }
  } else {
    protocolsBalances.push(protocolObject);
  }
};

const updateValues = async () => {
  const url = generateUrl(config.ADDRESSES);
  const eventSourceDict = generateEventSourceDict(config.ZAPPER_API_KEY);
  const eventSource = new EventSource(url, eventSourceDict);

  eventSource.addEventListener("open", () => {
    console.log("Open ...");
  });

  eventSource.addEventListener("balance", ({ data }) => {
    const parsedDatas = JSON.parse(data);
    const { appId } = parsedDatas;

    if (appId !== "nft") {
      if (appId === "tokens") {
        if (Object.keys(parsedDatas.balance.wallet).length > 0) {
          handleTokenBalance(parse(appId, parsedDatas.balance.wallet));
        }
      } else {
        let protocolObject = {
          key: appId,
          balanceUSD: parsedDatas.app.meta.total,
        };
        if (appId === "uniswap-v3") {
          protocolObject = {
            ...protocolObject,
            liquidityPositions: parse(appId, parsedDatas.app.data),
          };
        }
        handleProtocolPosition(protocolObject);
      }
    }
  });

  eventSource.addEventListener("end", async () => {
    const walletValue = getWalletValue(tokensBalances);
    const positionsValue = getPositionsValue(protocolsBalances);
    const totalBalanceUSD = walletValue + positionsValue;
    console.log("Total : ", parseInt(totalBalanceUSD).toLocaleString());
    console.log("Close.");
    eventSource.close();
    client.guilds.cache.map((guild) => {
      // update every server the bot is connected to
      if (guild.me) {
        guild.me.setNickname(`$${parseInt(totalBalanceUSD).toLocaleString()}`);
      }
    });
  });

  eventSource.addEventListener("error", ({ message }) => {
    console.log("Error : ", message);
  });
};

const run = async () => {
  // update once to init then run every hour
  await updateValues();
  setInterval(updateValues, 60 * 60000);
};

client.once("ready", () => {
  console.log("Ready!");
  client.user.setActivity("Abracadabra treasury", {
    type: "WATCHING",
  });
  run();
});

client.login(config.DISCORD_CLIENT_TOKEN);
