export const config = {
  ZAPPER_API_KEY: process.env.ZAPPER_API_KEY,
  DISCORD_CLIENT_TOKEN: process.env.DISCORD_CLIENT_TOKEN,
  ADDRESSES: ["0xdf2c270f610dc35d8ffda5b453e74db5471e126b"],
};
